﻿#pragma once
#include <wrl/client.h>
#include <dxgi1_6.h>
#include <d3d11_4.h>
#include <d2d1_3.h>
#include <dwrite_3.h>

class D2D1App {
public:
    // 构造函数
    D2D1App();
    // 析构函数
    ~D2D1App();
    // 可以重写此函数来实现你想做的每帧执行的操作
    void UpdateScene(float dt) {};
    // 渲染
    void DrawScene() {};
    // 处理消息
    static LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

    // 创建设备无关资源
    HRESULT CreateDeviceIndependentResources();
    // 创建设备有关资源
    HRESULT CreateDeviceResources(HWND hwnd, int width, int height);
    // 丢弃设备有关资源
    void DiscardDeviceResources();

    // 消息处理：鼠标
    void OnMouseDown(WPARAM btnState, int x, int y) {}
    void OnMouseUp(WPARAM btnState, int x, int y) {}
    void OnMouseMove(WPARAM btnState, int x, int y) {}
    void OnMouseWheel(UINT nFlags, short zDelta, int x, int y) {}

    Microsoft::WRL::ComPtr<IDWriteFactory> m_pDWriteFactory = nullptr;

    // D2D 设备上下文
    Microsoft::WRL::ComPtr <ID2D1DeviceContext> m_pD2DDeviceContext = nullptr;

    Microsoft::WRL::ComPtr<IDXGISwapChain1> m_pSwapChain = nullptr;

    // D2D 位图 储存当前显示的位图
    Microsoft::WRL::ComPtr <ID2D1Bitmap1> m_pD2DTargetBimtap = nullptr;
};


extern D2D1App* g_pD2DApp;