#pragma once
#include "TxtModelRole.h"

namespace txt
{
	struct sPet
	{
		uint small;
		uint big;
		uint dialog;
		uint small2;
		uint big2;
		uint dialog2;
		int_t e;
		// std::string name;
		sSound sound1;
		sSound sound2;
		struct sPoint
		{
			int offx;
			int atk1;
			int atk2;
			int atk3;
			int atk4;
			int magic;
		};
		sPoint point1;
		sPoint point2;
		std::vector<std::vector<int>> parts;
		struct sColorAct
		{
			uint color;
			sAction action;
		};
		std::vector<sColorAct> actions;

	};


	const vector<sPet>& getPets();

}


