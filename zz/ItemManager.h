#pragma once
#include "item.h"

class cFighter;
class cItemManager
{
public:
	cItemManager();

	bool Init();

	void resetFlag();

	int_t AddHpMpCp(const_int_t value, const_int_t max, int_t& target);

public:
	int_t m_temp_hp, m_temp_mp, m_temp_cp;

};

extern cItemManager g_ItemManager;