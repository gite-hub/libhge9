#pragma once
#include "rgb.h"

struct Vertex {
	float x, y; // screen position
	float z; // Z-buffer depth 0..1
	uint32_t col; // color
	float tx, ty; // texture coordinates
};


struct IDirect3DTexture9;

struct TextureQuad {

	float x, y, kx, ky, width, height, texture_width, texture_height;
	Vertex v[4];
	IDirect3DTexture9* tex;

	TextureQuad();

	void load(IDirect3DTexture9* texture);


	void setXY(int x, int y);

	void setRect(int kx, int ky, int width, int height);

	void update();

	void setAlpha(unsigned char alpha);

	void render();
};
