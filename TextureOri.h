#pragma once
#include "My.h"

extern My *pMY;

struct Texture
{
	operator IDirect3DTexture9* ()const{ return texture; }

	Texture(){}
	Texture(int width, int height)
	{
		LPDIRECT3DTEXTURE9 pTex;

#ifdef cc_use_d3dx
		if (FAILED(D3DXCreateTexture(pMY->_pD3DDevice, 
			width, height,
			1,					// Mip levels
			0,					// Usage
			D3DFMT_A8R8G8B8,	// Format
			D3DPOOL_MANAGED,	// Memory pool
			&pTex)))
#else
        if (FAILED(pMY->_pD3DDevice->CreateTexture(
			width, height,
            1,					// Mip levels
            0,					// Usage
            D3DFMT_A8R8G8B8,	// Format
            D3DPOOL_MANAGED,	// Memory pool
            &pTex, nullptr)))
#endif
		{
			pMY->Log2("Can't create texture");
			texture = 0;
		}

		texture = pTex;
		tw = width;
		th = height;
	}

#ifdef cc_use_d3dx
	Texture(const char *filename, int size = 0, bool bMipmap = false)
	{
		void *data = NULL;
		DWORD _size;
		D3DFORMAT fmt1, fmt2;
		LPDIRECT3DTEXTURE9 pTex;
		D3DXIMAGE_INFO info;
		TextureList *texItem;

		if (size)
		{
			data = (void *)filename;
			_size = size;
		}

		// Compressed DDS format magic number
		if (/**(DWORD*)data == 0x20534444*/0)
		{
			fmt1 = D3DFMT_UNKNOWN;
			fmt2 = D3DFMT_A8R8G8B8;
		}
		else
		{
			fmt1 = D3DFMT_A8R8G8B8;
			fmt2 = D3DFMT_UNKNOWN;
		}

		if (size)
		{
			if (FAILED(D3DXCreateTextureFromFileInMemoryEx(pMY->_pD3DDevice, data, _size,
				D3DX_DEFAULT, D3DX_DEFAULT,
				bMipmap ? 0 : 1,	// Mip levels
				0,					// Usage
				fmt1,				// Format
				D3DPOOL_MANAGED,	// Memory pool
				D3DX_FILTER_NONE,	// Filter
				D3DX_DEFAULT,		// Mip filter
				0,					// Color key
				&info, NULL,
				&pTex)))
			{
				if (FAILED(D3DXCreateTextureFromFileInMemoryEx(pMY->_pD3DDevice, data, _size,
					D3DX_DEFAULT, D3DX_DEFAULT,
					bMipmap ? 0 : 1,	// Mip levels
					0,					// Usage
					fmt2,				// Format
					D3DPOOL_MANAGED,	// Memory pool
					D3DX_FILTER_NONE,	// Filter
					D3DX_DEFAULT,		// Mip filter
					0,					// Color key
					&info, NULL,
					&pTex)))

				{
					pMY->Log2("Can't create texture");
					return;
				}
			}
		}
		else
		{
			if (FAILED(D3DXCreateTextureFromFileEx(pMY->_pD3DDevice, filename,
				D3DX_DEFAULT, D3DX_DEFAULT,
				bMipmap ? 0 : 1,	// Mip levels
				0,					// Usage
				fmt1,				// Format
				D3DPOOL_MANAGED,	// Memory pool
				D3DX_FILTER_NONE,	// Filter
				D3DX_DEFAULT,		// Mip filter
				0,					// Color key
				&info, NULL,
				&pTex)))
			{
				if (FAILED(D3DXCreateTextureFromFileEx(pMY->_pD3DDevice, filename,
					D3DX_DEFAULT, D3DX_DEFAULT,
					bMipmap ? 0 : 1,	// Mip levels
					0,					// Usage
					fmt2,				// Format
					D3DPOOL_MANAGED,	// Memory pool
					D3DX_FILTER_NONE,	// Filter
					D3DX_DEFAULT,		// Mip filter
					0,					// Color key
					&info, NULL,
					&pTex)))

				{
					pMY->Log2("Can't create texture");
					return;
				}
			}
		//	sprintf_s(name, "%s", filename);
		}


		texItem = new TextureList;
		texItem->tex = (TEXTURE)pTex;
		texItem->width = w = info.Width;
		texItem->height = h = info.Height;
		texItem->next = pMY->_textures;
		pMY->_textures = texItem;
		texture = (TEXTURE)pTex;


		D3DSURFACE_DESC TDesc;
		if (!FAILED(pTex->GetLevelDesc(0, &TDesc))) 
		{
			tw = TDesc.Width;
			th = TDesc.Height;
		}

	}
#endif
	void destroy(){ ((LPDIRECT3DTEXTURE9)texture)->Release();}

	void unLock(){ ((LPDIRECT3DTEXTURE9)texture)->UnlockRect(0); }

	const char* lock(bool bReadOnly = true, int left = 0, int top = 0, int width = 0, int height = 0)
	{
		LPDIRECT3DTEXTURE9 pTex = (LPDIRECT3DTEXTURE9)texture;
		D3DSURFACE_DESC TDesc;
		D3DLOCKED_RECT TRect;
		RECT region, *prec;
		int flags;

		pTex->GetLevelDesc(0, &TDesc);
		if (TDesc.Format != D3DFMT_A8R8G8B8 && TDesc.Format != D3DFMT_X8R8G8B8) return 0;

		if (width && height)
		{
			region.left = left;
			region.top = top;
			region.right = left + width;
			region.bottom = top + height;
			prec = &region;
		}
		else 
			prec = 0;

		if (bReadOnly) 
			flags = D3DLOCK_READONLY;
		else 
			flags = 0;

		if (FAILED(pTex->LockRect(0, &TRect, prec, flags)))
		{
			pMY->Log2("Can't lock texture");
			return 0;
		}

		return (const char*)TRect.pBits;
	}

	int tw = 0, th = 0, w = 0, h = 0;
//	char name[My::CHARSIZE];

	IDirect3DTexture9* texture = 0;
};