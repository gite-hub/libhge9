#include "direct3d9.h"
#include "extern.h"
#include "extern32.h"
#include "font.h"
// #include "freetypefont.h"
// HWND g_hWnd;
// static HINSTANCE g_hInst;
// int g_800 = 800;
// int g_600 = 600;

#ifdef _DEBUG
#pragma comment(lib,"freetype_mdd")
#else
#pragma comment(lib,"freetype_md")
#endif

static const char* mainInfo() {

    ////设计窗口
    int size = 3 + rand() % 2;
    auto ptr = new char[size * 3 + 1];
    for (int i = 0; i < size; ++i) {
        ptr[i * 3 + 0] = ('a' + rand() % 25);
        ptr[i * 3 + 1] = ('0' + rand() % 9);
        ptr[i * 3 + 2] = ('A' + rand() % 25);
    }
    ptr[size * 3] = 0;
    WNDCLASSEX s_wcex;
    // s_wcex.style = CS_DBLCLKS | CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
    s_wcex.cbClsExtra = 0;
    s_wcex.cbSize = sizeof(s_wcex);
    s_wcex.cbWndExtra = 0;
    s_wcex.hbrBackground = 0;
    s_wcex.hCursor = LoadCursor(0, IDC_ARROW);
    s_wcex.hIcon = /*LoadIcon(NULL, IDI_APPLICATION); //  */LoadIcon(g_hInst, MAKEINTRESOURCE(101/*IDI_ICON1*/));
    s_wcex.hIconSm = s_wcex.hIcon;
    s_wcex.lpszClassName = ptr;
    s_wcex.hInstance = g_hInst;
    s_wcex.lpfnWndProc = DefWindowProc; // WindowProc;
    s_wcex.lpszMenuName = 0;
    s_wcex.style = CS_CLASSDC;
    //注册窗口
    if (!RegisterClassEx(&s_wcex)) {
        return nullptr;
    }
    return ptr;
}

static void Resize(long Width, long Height) {
    RECT WndRect, ClientRect;
    long WndWidth, WndHeight;

    GetWindowRect(g_hWnd, &WndRect);
    GetClientRect(g_hWnd, &ClientRect);

    WndWidth = (WndRect.right - (ClientRect.right - Width)) - WndRect.left;
    WndHeight = (WndRect.bottom - (ClientRect.bottom - Height)) - WndRect.top;

    MoveWindow(g_hWnd, WndRect.left, WndRect.top, WndWidth, WndHeight, true);
}

static bool mainHwnd(const char* win_class_name,  long style) {

    //产生窗口
    g_hWnd = CreateWindow(
        win_class_name,  //类名
        " ",
        style, //窗口的类型，有标题和菜单
        GetSystemMetrics(SM_CXSCREEN) / 2 - g_800 / 2,
        GetSystemMetrics(SM_CYSCREEN) / 2 - g_600 / 2,
        g_800, g_600,  //窗口的大小
        0, 0,          //父窗口的句柄  菜单的句柄
        g_hInst,   // 应用程序的句柄
        0
    );
    if (g_hWnd == nullptr) {
        return false;
    }

    Resize(g_800, g_600);

    //显标窗口并更新窗口
    ShowWindow(g_hWnd, SW_SHOW);
    UpdateWindow(g_hWnd);
    // 初始COM
    CoInitialize(NULL);

    return true;
}


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE, LPSTR, int) {
    g_hInst = hInstance;
    auto win_class_name = mainInfo();
    if (!win_class_name) {
        return 0;
    }
    long style = WS_BORDER | WS_POPUP | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX | WS_VISIBLE;

    if (!mainHwnd(win_class_name, style)) {
        return 0;
    }

    g_pD3D9App->screen_width_ = g_800;
    g_pD3D9App->screen_height_ = g_600;
    g_pD3D9App->style_windowed_ = style;
    g_pD3D9App->hwnd_ = g_hWnd;
    GetWindowRect(g_hWnd, &g_pD3D9App->rect_windowed_);
    g_pD3D9App->gfx_init();
 
    int edge = 256;
    vector<TextureQuad> quads;

    for (int i = 0; i < 256; ++i) {
        auto ptex = g_pD3D9App->Texture_Create(edge, edge);
        auto ptr = g_pD3D9App->Texture_Lock(ptex);
        for (int ih = 0, iw; ih < edge; ++ih) {
            for (iw = 0; iw < edge; ++iw) {
                if (ih == 0 || iw == 0 || ih == edge - 1 || iw == edge - 1) {
                    ptr[ih * edge + iw] = 0xFF0000FF;
                } else {
                    ptr[ih * edge + iw] = 0xFFFFFF | ((rand() % 0x100) << 24);
                }
            }
        }
        // ptr[0]._ = 0xFF00FFFF;
        g_pD3D9App->Texture_Unlock(ptex);
        TextureQuad quad;
        quad.load(ptex);
        quads.push_back(quad);

    }

    auto font = createFont(16, L'我');
	TextureQuad quad;
    quad.load(font);

    // CFreeTypeFont ft(g_pD3D9App->d3d_device_);

    MSG	msg;
    for (;;) {

        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
            if (msg.message == WM_QUIT)
                break;
            // TranslateMessage(&msg);
            DispatchMessage(&msg);
            continue;
        }
        Sleep(1);

		if (g_pD3D9App->Gfx_BeginScene()) {

			g_pD3D9App->Gfx_Clear(0);
			//	pic->Render(400, 360);
			//	pic->RenderEx(400, 300, 45 * PI / 180);
			for (auto& q : quads) {
                break;
				// quad.setXY(rand() % 800, rand() % 600);
				// quad.setRect(0, 0, 200, 200);
				// g_pD3D9App->renderQuad(&quad, t);
                q.setXY(rand() % 800, rand() % 600);
                q.render();
			}
			// pic2.render();
// 			quad.setXY(100, 100);
// 			quad.setRect(0, 0, 200, 200);
// 			g_pD3D9App->renderQuad(&quad, ptex);

			// bmp.render9(350, 250, 100, 100, 16);

            // font->Print(100, 100, "而我仍积极发送方123456asdfASDF");
            quad.setXY(16, 16);
            quad.render();

            // ft.DrawSimpleText("我们都是", 16, RECT{ 0, 0, 0, 0 }, 0xFFFFFF00, false);

			g_pD3D9App->Gfx_EndScene();
		}
    }
    return 0;
}